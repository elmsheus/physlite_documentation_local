#!/usr/bin/env python

import os
import pandas as pd

# Define your directories for master csv files and generated csv files 
master_directory = "./master_csv_files"
output_directory = "./generated_csv_files"

# Specify the path to your log file
log_file = "log.txt"

# Open the log file in append mode
with open(log_file, "a") as logfile:
    message = f"Appending descriptions from master csv files into generated ones"
    print(message)
    logfile.write(message + "\n")

    # Iterate over each csv file in the output directory
    for output_filename in os.listdir(output_directory):
        if output_filename.endswith(".csv"):
            # Read the csv file from the output directory
            output_df = pd.read_csv(os.path.join(output_directory, output_filename))
            
            # Find the corresponding master file (master and generated csv file names are same)
            master_filename = output_filename
            master_filepath = os.path.join(master_directory, master_filename)

            try:
                # Read the corresponding master file
                # print (master_filepath) 
                master_df = pd.read_csv(master_filepath)

                # Loop over each row in the output DataFrame
                for index, row in output_df.iterrows():
                    # Find the corresponding row in the master DataFrame
                    matching_row = master_df[master_df['VariableName'] == row['VariableName']]

                    # If a matching row is found, extract the description and append it to the output DataFrame
                    if not matching_row.empty:
                        description = matching_row.iloc[0, 3]  # Description is the fourth column (index 3)
                        output_df.at[index, 'Description'] = description

                        # Check if description is empty in the master DataFrame
                        if pd.isna(description):
                            message = f"Description is empty in master file for {row['VariableName']} in {master_filename}"
                            print(message)
                            logfile.write(message + "\n")
                        else:
                           output_df.at[index, 'Description'] = description
                    else:
                        # Print the message to the log file
                        message = f"Matching variable is not found in master file for {row['VariableName']} in {master_filename}"
                        print(message)
                        logfile.write(message + "\n")

                # Save the updated output DataFrame back to the csv file
                output_df.to_csv(os.path.join(output_directory, output_filename), index=False)

            except FileNotFoundError:
                # Print the message to the log file
                message = f"Master file is not found for {output_filename}"
                print(message)
                logfile.write(message + "\n")

                if 'Description' not in output_df.columns:
                  # Add 'Description' column with empty values at index 3
                  output_df.insert(3, 'Description', '')
                  # Save the updated output DataFrame back to the csv file
                  output_df.to_csv(os.path.join(output_directory, output_filename), index=False)
                continue

    # Check if there are any "AnalysisTrigMatch_HLT_" files in the directory
    has_files_to_process = any(filename.startswith("AnalysisTrigMatch_HLT_") and filename.endswith(".csv") for filename in os.listdir(output_directory))
    # If there are no files to process, skip further processing 
    if not has_files_to_process:
       message = f"No 'AnalysisTrigMatch_HLT_' csv files in the {output_directory} directory. Either the sample does not have them or they are merged and appended already in AnalysisTrigMatch.csv on the fly, otherwise AnalysisTrigMatch.csv does not have master csv file. All done."
       print(message)
       logfile.write(message + "\n")
       message = f"Descriptions appended from master csv files successfully (any issue printed above)"
       logfile.write(message + "\n")
       print(message)
       exit()

    # Initialize the list to store individual DataFrames
    combined_dfs = []

    # Loop over each file in the output directory to merge AnalysisTrigMatch_HLT files
    for filename in os.listdir(output_directory):
       if filename.startswith("AnalysisTrigMatch_HLT_") and filename.endswith(".csv"):
          # Read the csv file
          df = pd.read_csv(os.path.join(output_directory, filename))

          # Remove the file extension from the filename to use as 'VariableName'
          variable_name = os.path.splitext(filename)[0]

          # Set the 'VariableName' column
          df['VariableName'] = variable_name

          # Insert the 'Description' column with the specified value
          if 'Description' not in df.columns:
            df.insert(3, 'Description', 'Link to the object matched for the given trigger pattern')
          else:
            df['Description'] = 'Link to the object matched for the given trigger pattern'

          # Append the DataFrame to the list of combined DataFrames
          combined_dfs.append(df)

    # Concatenate all DataFrames in the list into a single DataFrame
    combined_df = pd.concat(combined_dfs)

    # Reset the LineNumber column to consecutive integers starting from 1
    combined_df['LineNumber'] = range(1, len(combined_df) + 1)

    # Save the combined DataFrame to a single csv file
    combined_df.to_csv(os.path.join(output_directory, "AnalysisTrigMatch.csv"), index=False)

    # Loop over each file in the output directory again
    for filename in os.listdir(output_directory):
      if filename.startswith("AnalysisTrigMatch_HLT_") and filename.endswith(".csv"):
        # Construct the file path
        file_path = os.path.join(output_directory, filename)
        # Delete the file
        os.remove(file_path)

    message = f"'AnalysisTrigMatch_HLT_' csv files do not have master csv files, they are merged into one in AnalysisTrigMatch.csv with their descriptions set to 'Link to the object matched for the given trigger pattern' on the fly and deleted"
    print(message)
    logfile.write(message + "\n")

    message = f"Descriptions appended from master csv files successfully (any issue printed above)"
    logfile.write(message + "\n")
    
print(message)
