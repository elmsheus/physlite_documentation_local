# Physlite Documentation

Scripts to make documentation for PHYSLITE variables

## Execution:

```
setupATLAS
lsetup "root 6.30.02-x86_64-el9-gcc13-opt" 
python3 getVariables.py --inputFile PHYSLITE_FILENAME
python3 appendDescriptions.py
python3 createHTML.py --outputFile HTMLFILENAME --sampleName mytest
```

or use the local script (adapt `PHYSLITEFILE` and `HTMLFILE`)
```
run.sh
```

The acrontab script `runcron.sh` uses the daily ART outputs of the `TrfTestsARTPlots` (https://gitlab.cern.ch/atlas/athena/-/tree/main/Tools/TrfTestsARTPlots/test?ref_type=heads) in EOS and produces HTML webpages based on the current 5 tests at https://elmsheus.web.cern.ch/physlite_variables/index.html:
```
runcron.sh
```


## Details:

* `getVariables.py`: opens a PHYSLITE root file, gets the branches from CollectionTree, 
filters only AuxDyn branches as they are the variables, creates a pandas dataframe, 
extracts the variable names, creates a directory called generated_csv_files 
to save csv files, for each AuxDyn branche the variable name and its type is saved 
into a csv file (variable name is sorted alphabetically ignoring case). 
Example: from the branch AnalysisElectronsAuxDyn.eta, it extracts the variable name 
as "eta" and saves into a file AnalysisElectrons.csv in generated_csv_files 
directory. 

* `appendDescriptions.py`: master csv files are made by hand from the spreadsheet, 
have to be stored in git repo. Loops over each csv files in generated_csv_files
directory and compares with its master csv file (csv files names are same as they 
are in different directories), it finds the variable's description from the master and 
appends it to the generated one. Exceptions are defined. Descriptions are appended 
into the same csv files, no new file names are defined.

* `createHTML.py`: makes a list of csv files from generated_csv_files directory and makes 
html tables for all csv files with style. Copies html file into a dedicated web area.

*  a log file is appended with info on each execution of the three python files 

## Before you start:

* download a PHYSLITE root file (DAOD_PHYSLITE.37223155._000397.pool.root.1)
* remove log.txt 
* remove generated_csv_files directory
* define the path to your local root file and to the relevant directories in python files
