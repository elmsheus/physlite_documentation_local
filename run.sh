#!/bin/bash

#PHYSLITEFILE="../trftestsrtplots_20240410/run_q442/DAOD_PHYSLITE.art.pool.root"
PHYSLITEFILE="../trftestsrtplots_20240410/run_q454/DAOD_PHYSLITE.art.pool.root"

#HTMLFILE="physlite_variables_q442.html"
HTMLFILE="physlite_variables_q454.html"

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup "root recommended" 

python getVariables.py --inputFile $PHYSLITEFILE
python appendDescriptions.py

mkdir art-physlite-variables
python createHTML.py --outputFile $HTMLFILE --sampleName q454

