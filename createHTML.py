#!/usr/bin/env python

import pandas as pd
import os, sys
import shutil
import argparse

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--outputFile', type=str)
    parser.add_argument('--sampleName', type=str)
    args = parser.parse_args()
 
    if len(sys.argv) < 3:
        parser.print_help()
        sys.exit(1)

    # Directory containing the generated/appended csv files
    csv_files_directory = "./generated_csv_files"

    # html file destination www directory
    #destination_directory = '/eos/atlas/atlascerngroupdisk/proj-amg/www/art-physlite-variables/'
    destination_directory = "art-physlite-variables"
    os.makedirs(destination_directory, exist_ok=True)

    # Write html to a file
    #output_html_path = 'physlite_variables_mc23_p6026.html'
    output_html_path = args.outputFile

    # Specify the path to your log file
    log_file = "log.txt"

    # Dictionary to store csv file names and custom titles
    csv_titles = {}

    # Iterate over each file in the directory
    for filename in os.listdir(csv_files_directory):
        # Check if the file is a csv file
        if filename.endswith('.csv'):
            # Extract the custom title from the filename
            custom_title = os.path.splitext(filename)[0]
            # Add the filename and custom title to the dictionary
            csv_titles[filename] = custom_title

    # Print the generated dictionary
    # print(csv_titles)

    # Define common custom column names for all csv files for html tables
    common_custom_columns = ['Variable Name', 'Type', 'Description']  

    # List of csv file names (sorted alphabetically)
    csv_files = sorted(csv_titles.keys(), key=str.lower)

    # Generate html tables with custom titles and column names for each csv file
    html_tables = []
    title_links = []  # List to store clickable title links
    for file in csv_files:
        print(file)
        # Construct the full path to the csv file
        file_path = os.path.join(csv_files_directory, file)
        # Read csv file into DataFrame
        df = pd.read_csv(file_path)

        # Replace NaN values with "N/A" or ""
        df = df.fillna("")

        # Get the custom title for the current csv file
        custom_title = csv_titles.get(file, f"Untitled ({file})")

        # Drop the line numbers from html tables
        df = df.drop(columns=['LineNumber'])

        # Use common custom column names for all csv files
        df.columns = common_custom_columns

        # Convert DataFrame to html HTML
        html_table = df.to_html(index=False, classes='table table-striped table-bordered', table_id=file)

        # Add custom title link to the list
        title_link = f'<a href="#{file}">{custom_title}</a>'
        title_links.append(title_link)

        # Add custom title and table to the HTML tables list
        table_with_title = f"""
        <div class="table-container">
            <h6 id="{file}">
                {custom_title}
                <a class="back-to-top" href="#" onclick="scrollToTop(); return false;">[back to top]</a>
            </h6>
            {html_table}
        </div>
        """
        html_tables.append(table_with_title)

    # html template with Bootstrap styling and custom CSS for left-aligned column names
    html_template = f"""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Documentation on PHYSLITE Variables</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <style>
            body {{
                padding: 15px;
            }}
            th {{
                text-align: left !important;
            }}
            .table {{
                table-layout: fixed;
            }}
            .table th,
            .table td {{
                width: 50px; /* Adjust as needed */
                word-wrap: break-word; /* Wrap long text within columns */
                font-size: 14px; /* Adjust font size as needed */
                padding: 2px; /* Adjust padding to control row height */
                padding-left: 6px; /* Add padding to the left side of cells */
            }}
            .table thead tr:first-child {{ 
               background-color: cornflowerblue; /* Apply blue background to the first row (header row) of each table */
               color: white; /* Optional: Change text color to white for better contrast */
            }}
            .title-links {{
                overflow: auto;
                margin-bottom: 10px; /* Add margin after the title links */
            }}
            .title-link {{
                display: inline-block;
                margin-right: 8px;
                border-right: 3px solid #ddd; /* Add a right border for separation */
                padding-right: 6px; /* Optional: Add padding for better visual separation */
                margin-bottom: 6px;
            }}
            .table {{
                margin-bottom: 20px; /* Optional: Add margin to separate tables */
            }}
            .table-container {{
                position: relative;
            }}
            .back-to-top {{
                position: absolute;
                top: 0;
                right: 10;
                font-size: 12px;
                text-decoration: none;
            }}
        </style>
    </head>
    <body>

    <div class="container">
    <h3>Documentation on PHYSLITE Variables</h3>
    <p style="font-size: 14px;">Page generated from sample: {args.sampleName}</p>
    <h6>List of Containers:</h6>
    <!-- Display clickable title links at the beginning of the webpage -->
      <nav class="navbar navbar-light bg-light">
          <div class="title-links">
              {''.join(f'<div class="title-link">{link}</div>' for link in title_links)}
          </div>
      </nav>
      <!-- Display HTML tables with custom titles and column names for each CSV file -->
      {''.join(html_tables)}
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        // Add smooth scroll to the "Back to Top" links
        function scrollToTop() {{
            document.body.scrollIntoView({{
                behavior: 'smooth'
            }});
        }}
    </script>

    </body>
    </html>
    """

    # Write html to a file
    with open(output_html_path, 'w') as html_file:
        html_file.write(html_template)

    # Open the log file in append mode
    with open(log_file, "a") as logfile:
        message = f"Styled html file generated successfully"
        print(message)
        logfile.write(message + "\n")
        
        # Copy the html file to destination www directory
        try:
            shutil.copy(output_html_path, destination_directory)
            message = f"html file copied to destination_directory successfully"
            print(message)
            logfile.write(message + "\n")
        except Exception as e:
            raise RuntimeError(f"Error copying html file to destination directory: {e}")
            message = f"Error copying html file to destination directory: {e}"
            print(message)
            logfile.write(message + "\n")

if __name__ == "__main__":
    ret = main()
    sys.exit(ret)
