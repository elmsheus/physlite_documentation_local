#!/bin/bash

# Working directory of the acrontab job
cd /afs/cern.ch/user/e/elmsheus/physlite_documentation

# Setup ROOT from CVMFS so that uproot and pandas is available
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root recommended" 

# Clean-up left-overs from previous test
rm -rf art-physlite-variables
rm -rf generated_csv_files
rm log.txt
rm generated_csv_files.tar.gz

# EOS location to store HTML output files
wwwdir=/eos/user/e/elmsheus/www/physlite_variables

# ART test parameters
datestamp=`date -d "yesterday 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-2 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-3 day 21:01" '+%Y-%m-%dT%H%M'`

arch="x86_64-el9-gcc13-opt"
echo "Using $datestamp and $arch"

# EOS location of ART all output files
eostestoutput="/eos/atlas/atlascerngroupdisk/data-art/grid-output/main/Athena"

# ART test names to process
testnames=( test_trf_q442_phys_physlite_ca
            test_trf_q443_phys_physlite_ca
            test_trf_q445_phys_physlite_ca
            test_trf_q449_phys_physlite_ca
            test_trf_q454_phys_physlite_ca )

#testnames=( test_trf_q454_phys_physlite_ca )

date > log.txt

# Create local HTML output directory
mkdir art-physlite-variables

# Loop over the ART test output directories and extract generated_csv_files.tar.gz
for i in "${testnames[@]}"
do
    # Clean up previous generated_csv_files directory
    rm $i.html
    rm -rf generated_csv_files
    rm generated_csv_files.tar.gz

    # extract generated_csv_files.tar.gz from ART test tarball output on EOS
    echo "===== tar xf $eostestoutput/$arch/$datestamp/TrfTestsARTPlots/$i*/*EXT*.tar generated_csv_files.tar.gz"
    tar xf $eostestoutput/$arch/$datestamp/TrfTestsARTPlots/$i*/*EXT*.tar generated_csv_files.tar.gz
    tar xzf generated_csv_files.tar.gz
    echo "============="

    # Append the varibable descriptions
    echo "===== python appendDescriptions.py"
    python appendDescriptions.py
    echo "============="

    # Create HTML file
    echo "===== python createHTML.py --outputFile HTMLFILE=$i.html --sampleName $i on `date` from nightly Athena,main,$datestamp"
    python createHTML.py --outputFile $i.html --sampleName "$i on `date` from nightly Athena,main,$datestamp"
    echo "============="

done

# Copy HTML files to EOS location
echo "===== cp art-physlite-variables/*.html $wwwdir"
cp art-physlite-variables/*.html $wwwdir
cp log.txt $wwwdir
echo "===== done"
